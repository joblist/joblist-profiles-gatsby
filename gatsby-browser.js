import "./src/styles/index.css";
import "@joblist/components";
import React from "react";
import SiteLayout from "./src/components/site/layout";

export const wrapRootElement = ({ element, props }) => {
	return <SiteLayout {...props}>{element}</SiteLayout>;
};
