import React from "react";

function sortIndex(a, b) {
	return a > b;
}

export default function IndexList({ index = [], children }) {
	return Object.keys(index)
		.sort(sortIndex)
		.map((indexLetter) => {
			const indexTerms = index[indexLetter];
			return (
				<section id={`index-${indexLetter}`} key={indexLetter}>
					<h2>
						<a href={`#index-${indexLetter}`}>{indexLetter}</a>
					</h2>
					{children && children(indexTerms)}
				</section>
			);
		});
}
