import React, { useEffect, useRef } from "react";
import { graphql } from "gatsby";

import CompanyCard from "./card";

export default function CompanyPageTemplate({ data: { company } }) {
	return (
		<>
			<CompanyCard company={company} full={true} />
			<joblist-to text="Explore the full profile on">
				<a href={`https://joblist.today/${company.fields.slug}`}>
					joblist.today/{company.fields.slug}
				</a>
			</joblist-to>
		</>
	);
}

export const query = graphql`
	query ($slug: String!) {
		company: markdownRemark(
			fields: { slug: { eq: $slug } }
			fileAbsolutePath: { regex: "//(companies)/" }
		) {
			...CompanyCardFragment
		}
	}
`;
