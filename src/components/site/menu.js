import React from "react";
import { useStaticQuery, graphql, Link } from "gatsby";

export default function SiteNav() {
	const data = useStaticQuery(graphql`
		query HeaderQuery {
			site {
				siteMetadata {
					title
				}
			}
		}
	`);
	const {
		siteMetadata: { title: siteTitle = "~/" },
	} = data.site;

	return (
		<joblist-menu>
			<menu>
				<li>
					<Link to={`/companies`} title="Explore all companies">
						companies
					</Link>{" "}
				</li>
				<li>
					<Link
						to={`/tags`}
						title="Explore all tags used across the content collections"
					>
						tags
					</Link>
				</li>
			</menu>
			<menu>
				<a
					href={"https://edit.joblist.today/#/collections/companies/new"}
					title="Add a new company (free — github)"
				>
					new company
				</a>
			</menu>
		</joblist-menu>
	);
}
