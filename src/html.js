import React from "react";
import PropTypes from "prop-types";

export default function HTML(props) {
	return (
		<html joblist-layout="true" {...props.htmlAttributes}>
			<head>
				<meta charSet="utf-8" />
				<meta httpEquiv="x-ua-compatible" content="ie=edge" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1, shrink-to-fit=no"
				/>
				{props.headComponents}
			</head>
			<body {...props.bodyAttributes} className="site">
				{props.preBodyComponents}
				<joblist-layout
					key={`body`}
					id="___gatsby"
					className="site__gatsby"
					dangerouslySetInnerHTML={{ __html: props.body }}
				/>
				{props.postBodyComponents}
			</body>
		</html>
	);
}
