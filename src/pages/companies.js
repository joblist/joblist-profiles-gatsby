import React from "react";
import { graphql, Link } from "gatsby";

import CompanyList from "../components/company/list";
import IndexList from "../components/index/list.js";
import IndexToc from "../components/index/toc.js";

export default function CompaniesPage({ data }) {
	const companies = data?.companies?.edges;

	/* build an alphabetic index of companies */
	const companyIndex = companies?.reduce((index, company) => {
		const { slug } = company.node.fields;
		const indexLetter = slug.slice(0, 1).toLowerCase();
		index[indexLetter] = index[indexLetter] || [];
		index[indexLetter].push(company);
		return index;
	}, {});

	return (
		<>
			<header className="page__header">
				<h1>
					<Link to={"/companies"}>Companies</Link>
				</h1>
				<p>
					Listing <strong>{companies.length}</strong>{" "}
					<Link to={"/companies"}>companies</Link>, also explorable by{" "}
					<Link to={"/tags/companies"}>tags</Link>.
				</p>
			</header>
			<main className="page__main page__main--index">
				<menu className="nav nav--index">
					<IndexToc index={companyIndex}></IndexToc>
				</menu>
				<main className="page__content">
					<IndexList index={companyIndex}>
						{(indexTerms) => <CompanyList list={indexTerms} />}
					</IndexList>
				</main>
			</main>
		</>
	);
}

export const query = graphql`
	{
		companies: allMarkdownRemark(
			filter: { fileAbsolutePath: { regex: "//(companies)/" } }
			sort: { fields: [frontmatter___title], order: [ASC] }
		) {
			edges {
				node {
					...CompanyCardFragment
				}
			}
		}
	}
`;
