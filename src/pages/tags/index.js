import React from "react";
import { Link } from "gatsby";

export default function TagsIndexPage() {
	return (
		<div className="site_page page">
			<header className="page__header">
				<h1 className="page__title">Tags</h1>
				<p>
					Explore <Link to={"/tags"}>tags</Link> by content collection
				</p>
			</header>
			<main className="page__main">
				<nav className="page__nav nav nav--collections">
					<ul>
						<li className="nav__item">
							<Link to={`/tags/companies`}>companies</Link>
						</li>
					</ul>
				</nav>
			</main>
		</div>
	);
}
