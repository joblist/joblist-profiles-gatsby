# joblist-profiles-gatsby

Sourcing data from `git@github.com:joblisttoday/data.git`, and
rendering it as a static site.

## Development

- `npm install` to install the dependencies
- `npm run dev` to run the local dev server

This website is using gatsby.js

